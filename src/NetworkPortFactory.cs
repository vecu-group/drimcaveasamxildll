﻿
using ASAM.XIL.Interfaces.Testbench.Common.WatcherHandling;

using ASAM.XIL.Interfaces.Testbench.NetworkPort;

//using ASAM.XIL.Interfaces.Testbench.MAPort;
//using ASAM.XIL.Interfaces.Testbench.MAPort.Enum;

namespace DrimcaveXilAPI
{
    public class NetworkPortFactory : INetworkPortFactory
    {
        public IFrameWatcher CreateFrameWatcher()
        {
            throw new System.NotImplementedException();
        }

        public INetworkPort CreateNetworkPort(string name)
        {
            var port = new NetworkPort { Name = name };
            return port;
        }
    }
}
