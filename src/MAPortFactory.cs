﻿
using ASAM.XIL.Interfaces.Testbench.Common.WatcherHandling;
using ASAM.XIL.Interfaces.Testbench.MAPort;
using ASAM.XIL.Interfaces.Testbench.MAPort.Enum;


namespace DrimcaveXilAPI
{
    internal class MAPortFactory : IMAPortFactory
    {
        public IMAPort CreateMAPort(string name)
        {
            var port = new MAPort();
            return port;
        }

        public int CreateMAPortBreakpoint(IWatcher watcher, BreakpointAction action)
        {
            throw new System.NotImplementedException();
        }

        public IMAPortBreakpoint CreateMAPortBreakpoint2(IWatcher watcher, BreakpointAction action)
        {
            throw new System.NotImplementedException();
        }
    }
}
