﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASAM.XIL.Interfaces.Testbench.NetworkPort;
using ASAM.XIL.Interfaces.Testbench.NetworkPort.Enum;


namespace DrimcaveXilAPI
{
    public class Frame : IFrame
    {
        public Frame (Channel ch)
        {
            sender = new Sender(this);
            receiver = new Receiver(this);
        }
        public IFrameValue CustomValue 
        { 
            get
            {
                return customValue;
            }
            set
            {
                customValue = value;
            }
        }
        IFrameValue customValue = new FrameValue();
        public byte DLC
        {
            get
            {
                return CustomValue.DLC;
            }
            set
            {
                CustomValue.DLC = value;
            }
        }

        public int ID
        {
            get
            {
                return CustomValue.ID;
            }
            set
            {
                CustomValue.ID = value;
            }
        }

        public string Name { get; set; }


        public ISender Sender
        {
            get
            {
                return sender;
            }
        }
        Sender sender;

        public IReceiver Receiver
        {
            get
            {
                return receiver;
            }
        }

        Receiver receiver;

    public string ToString2()
        {
            string hex = String.Format("0x{0:X2} 0x{1:X2} 0x{2:X2} 0x{3:X2} 0x{4:X2} 0x{5:X2} 0x{6:X2} 0x{7:X2}"
                , CustomValue.DataBytes[0], CustomValue.DataBytes[1]
                , CustomValue.DataBytes[2], CustomValue.DataBytes[3]
                , CustomValue.DataBytes[4], CustomValue.DataBytes[5]
                , CustomValue.DataBytes[6], CustomValue.DataBytes[7]);

            //return String.Format(":{0},0x{1:x},8,{2}", CustomValue.Timestamp, CustomValue.ID,hex);
            return String.Format("0x{0:x},8,{1}", CustomValue.ID, hex);
        }
        public string ToString3()
        {
            string hex = String.Format("0x{0:X2} 0x{1:X2} 0x{2:X2} 0x{3:X2} 0x{4:X2} 0x{5:X2} 0x{6:X2} 0x{7:X2}"
                , CustomValue.DataBytes[0], CustomValue.DataBytes[1]
                , CustomValue.DataBytes[2], CustomValue.DataBytes[3]
                , CustomValue.DataBytes[4], CustomValue.DataBytes[5]
                , CustomValue.DataBytes[6], CustomValue.DataBytes[7]);

            //return String.Format(":{0},0x{1:x},8,{2}", CustomValue.Timestamp, CustomValue.ID,hex);
            return String.Format("0x{0:x}_8_{1}", CustomValue.ID, hex);
        }


        #region Not Implemented

        public IChannel Channel => throw new NotImplementedException();

        public IList<IPdu> Pdus => throw new NotImplementedException();


        public IFrameValue RxValue => throw new NotImplementedException();


        public IFrameValue TxValue => throw new NotImplementedException();

        public IPdu AddNewPdu(long startBit, long byteLength, string name)
        {
            throw new NotImplementedException();
        }

        public void DeletePdu(IPdu pdu)
        {
            throw new NotImplementedException();
        }

        public IPdu GetPdu(string pduName)
        {
            throw new NotImplementedException();
        }

        public ISignal GetSignal(string pathName)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
