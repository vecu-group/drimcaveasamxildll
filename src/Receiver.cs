﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASAM.XIL.Interfaces.Testbench.NetworkPort;

namespace DrimcaveXilAPI
{
    class Receiver : IReceiver
    {
        Frame parent;
        public Receiver(Frame f)
        {
            parent = f;
            isActive = false;
        }
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }

        bool isActive;

        public IFrameValue SingleReceive(double timeout)
        {
            isActive = false;
            return parent.CustomValue;
        }


        #region Not Implemented

        public bool IsStarted => throw new NotImplementedException();

        public void StartReceiving()
        {
            throw new NotImplementedException();
        }

        public void StopReceiving()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
