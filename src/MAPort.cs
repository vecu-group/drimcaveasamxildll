﻿
using System;
using System.Collections.Generic;
using ASAM.XIL.Implementation.Testbench.Common.ValueContainer;
using ASAM.XIL.Interfaces.Testbench.Common.Capturing;
using ASAM.XIL.Interfaces.Testbench.Common.MetaInfo;
using ASAM.XIL.Interfaces.Testbench.Common.SignalGenerator;
using ASAM.XIL.Interfaces.Testbench.Common.TargetScript;
using ASAM.XIL.Interfaces.Testbench.Common.ValueContainer;
using ASAM.XIL.Interfaces.Testbench.Common.ValueContainer.Enum;
using ASAM.XIL.Interfaces.Testbench.Common.VariableRef;
using ASAM.XIL.Interfaces.Testbench.MAPort;
using ASAM.XIL.Interfaces.Testbench.MAPort.Enum;

namespace DrimcaveXilAPI
{
    internal class MAPort : IMAPort
    {
        //string canoeCfgPath;
        private CANoe.Application mCANoeApp;
        private CANoe.Measurement mCANoeMeasurement;
        public MAPort()
        {
        }

        public IMAPortConfig LoadConfiguration(string filepath)
        {
            mCANoeApp = new CANoe.Application();
            mCANoeMeasurement = (CANoe.Measurement)mCANoeApp.Measurement;

            //string mAbsoluteConfigPath = System.IO.Path.GetFullPath(filepath);
            if (mCANoeMeasurement.Running)
            {
                mCANoeMeasurement.Stop();
            }

            if (mCANoeApp != null)
            {
                mCANoeApp.Open(filepath, true, true);
                CANoe.OpenConfigurationResult ocresult = mCANoeApp.Configuration.OpenConfigurationResult;
                if (ocresult.result == 0)
                {

                }
            }
            return null;
        }


        public void StartSimulation()
        {
            if (mCANoeMeasurement != null)
            {
                if (!mCANoeMeasurement.Running)
                {
                    mCANoeMeasurement.Start();
                }
            }
        }

        public void StopSimulation()
        {
            if (mCANoeMeasurement != null)
            {
                if (mCANoeMeasurement.Running)
                {
                    mCANoeMeasurement.Stop();
                }
            }
        }

        public void Disconnect()
        {
            if(mCANoeApp != null)
                mCANoeApp.Quit();
        }


        #region Not Implemented
        public IBaseValue Read(string variableName)
        {
            return null;
        }

        public void Write(string variableName, IBaseValue value)
        {

        }

        public IMAPortBreakpoint Breakpoint { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public IMAPortConfig Configuration => throw new NotImplementedException();

        public double DAQClock => throw new NotImplementedException();

        public double SimulationStepSize => throw new NotImplementedException();

        public SimultaneousLevel SimultaneousLevel => throw new NotImplementedException();

        public MAPortState State => throw new NotImplementedException();

        public IList<ITaskInfo> TaskInfos => throw new NotImplementedException();

        public IList<string> TaskNames => throw new NotImplementedException();

        public IList<string> VariableNames => throw new NotImplementedException();

        public string Name => throw new NotImplementedException();

        public IList<string> CheckVariableNames(IList<string> variableNames)
        {
            throw new NotImplementedException();
        }

        public IList<IVariableRef> CheckVariableRefs(IList<IVariableRef> variableRefs)
        {
            throw new NotImplementedException();
        }

        public void Configure(IMAPortConfig config, bool forceConfig)
        {
            throw new NotImplementedException();
        }

        public ICapture CreateCapture(string taskName)
        {
            throw new NotImplementedException();
        }

        public ISignalGenerator CreateSignalGenerator()
        {
            throw new NotImplementedException();
        }

        public ITargetScript CreateTargetScript()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void DownloadParameterSets(IList<string> filepaths)
        {
            throw new NotImplementedException();
        }

        public DataType GetDataType(string variableName)
        {
            throw new NotImplementedException();
        }

        public IMAPortVariableInfo GetVariableInfo(string variableName)
        {
            throw new NotImplementedException();
        }

        public bool IsReadable(string variableName)
        {
            throw new NotImplementedException();
        }

        public bool IsWritable(string variableName)
        {
            throw new NotImplementedException();
        }


        public void PauseSimulation()
        {
            throw new NotImplementedException();
        }

        public IBaseValue Read2(IVariableRef variableRef)
        {
            throw new NotImplementedException();
        }

        public IList<IBaseValue> ReadSimultaneously(IList<string> variableNames, string taskName)
        {
            throw new NotImplementedException();
        }

        public IList<IBaseValue> ReadSimultaneously2(IList<IVariableRef> variableRefs, string taskName)
        {
            throw new NotImplementedException();
        }


        public void WaitForBreakpoint(double timeout)
        {
            throw new NotImplementedException();
        }

        public void Write2(IVariableRef variableRef, IBaseValue value)
        {
            throw new NotImplementedException();
        }

        public void WriteSimultaneously(IList<string> variableNames, IList<IBaseValue> values, string taskName)
        {
            throw new NotImplementedException();
        }

        public void WriteSimultaneously2(IList<IVariableRef> variableRefs, IList<IBaseValue> values, string taskName)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
