﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using ASAM.XIL.Interfaces.Testbench.Common.SignalGenerator;
using ASAM.XIL.Interfaces.Testbench.Common.ValueContainer;
using ASAM.XIL.Interfaces.Testbench.NetworkPort;
using ASAM.XIL.Interfaces.Testbench.NetworkPort.Enum;

namespace DrimcaveXilAPI
{
    public class NetworkPort : INetworkPort
    {
        public NetworkPort()
        {
            State = NPState.eDISCONNECTED;
            clusters.Add(new Cluster(this));
        }

        public IList<ICluster> Clusters { 
            get
            {
                return clusters;
            }
        }

        private IList<ICluster> clusters = new List<ICluster>();
        public NPState State { get; set; }

        TcpClient tc;
        static public NetworkStream TcpStream;
        TcpClient tcAVT;
        static public NetworkStream TcpStreamAVT;

        private const int tcpPortTestFlag = 0;
        private const int tcpPortAvtState = 11190 + tcpPortTestFlag;
        private const int tcpPortAvtComm = 11180 + tcpPortTestFlag;
        public void Start()
        {
            try
            {
                tcAVT = new TcpClient("127.0.0.1", tcpPortAvtState);
                TcpStreamAVT = tcAVT.GetStream();
            }
            catch (Exception e)
            {
                State = NPState.eDISCONNECTED;
                return;
            }
            State = NPState.eSTOPPED;
            SendStatus("1,1,1,[ASAMXIL] INetworkPort Start();");
            ConnectedAVT();
        }

        public void Stop()
        {
            SendStatus("1,1,1,[ASAMXIL] INetworkPort Stop();");
            TcpStream.Close();
            tc.Close();
            TcpStreamAVT.Close();
            tcAVT.Close();
            State = NPState.eDISCONNECTED;
        }

        public void Disconnect()
        {
            TcpStream.Close();
            tc.Close();
            TcpStreamAVT.Close();
            tcAVT.Close();
            State = NPState.eDISCONNECTED;
        }

        void ConnectedAVT()
        {
            Thread thread = new Thread(new ThreadStart(runWaitRun));
            thread.IsBackground = true;
            thread.Start();
        }

        void runWaitRun()
        {
            Boolean isReceiveStatus = false;
            byte[] receiveBuf = new byte[1024];
            string msg = "1,2,2,1;";
            string checkmsg = "1,2,2,2";            
            SendStatus(msg);            
            while (isReceiveStatus == false)
            {
                int nbytes = TcpStreamAVT.Read(receiveBuf, 0, receiveBuf.Length);
                string output = Encoding.ASCII.GetString(receiveBuf, 0, nbytes);
                string[] arr = output.Split(';');
                foreach (string s in arr)
                {
                    if(checkmsg.Equals(s))
                    {
                        isReceiveStatus = true;
                        break;
                    }
                }
            }
            Console.WriteLine("Receive Status");

            try
            {
                tc = new TcpClient("127.0.0.1", tcpPortAvtComm);
                TcpStream = tc.GetStream();
            }
            catch (Exception e)
            {
                State = NPState.eDISCONNECTED;
                return;
            }
            State = NPState.eRUNNING;
        }
        public static void SendStatus(string msg)
        {
            byte[] sendBuf = Encoding.ASCII.GetBytes(msg);
            TcpStreamAVT.Write(sendBuf, 0, sendBuf.Length);
        }
        #region Not Implemented
        public string Name { get; set; }

        public double DAQClock => throw new NotImplementedException();

        public IFrameCapture CreateFrameCapture()
        {
            throw new NotImplementedException();
        }

        public ISignalCapture CreateSignalCapture()
        {
            throw new NotImplementedException();
        }

        public ISignalGenerator CreateSignalGenerator()
        {
            throw new NotImplementedException();
        }

        public ISignalGenerator CreateSignalGenerator2()
        {
            throw new NotImplementedException();
        }

        public IBaseValue Read(string signalName)
        {
            throw new NotImplementedException();
        }

        public void Write(string signalName, IBaseValue value)
        {
            throw new NotImplementedException();
        }

        public IFrame GetFrameByIdAndPathName(int id, string pathName)
        {
            throw new NotImplementedException();
        }

        public IFrame GetFrameByPathName(string pathName)
        {
            throw new NotImplementedException();
        }

        public IPdu GetPdu(string pathName)
        {
            throw new NotImplementedException();
        }

        public ISignal GetSignal(string pathName)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public ICluster GetCluster(string clusterName)
        {
            throw new NotImplementedException();
        }


        public INetworkPortConfig Configuration { get; set; }
        public void Configure(INetworkPortConfig config)
        {

        }
        public INetworkPortConfig LoadConfiguration(string filePath)
        {
            return null;
        }

        public IChannel GetChannel(string pathName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
