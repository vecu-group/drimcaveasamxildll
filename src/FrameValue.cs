﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASAM.XIL.Interfaces.Testbench.Common.ValueContainer;
using ASAM.XIL.Interfaces.Testbench.Common.ValueContainer.Enum;
using ASAM.XIL.Interfaces.Testbench.NetworkPort;
using ASAM.XIL.Interfaces.Testbench.NetworkPort.Enum;

namespace DrimcaveXilAPI
{
    public class FrameValue : IFrameValue
    {
        public FrameValue()
        {
            DLC = 8;
            ID = 0;
            Timestamp = 0;
            Type = DataType.eUINT;
            DataBytes = dataBytes;
            dataBytes.Add(0);
            dataBytes.Add(0);
            dataBytes.Add(0);
            dataBytes.Add(0);
            dataBytes.Add(0);
            dataBytes.Add(0);
            dataBytes.Add(0);
            dataBytes.Add(0);
        }

        public IList<byte> DataBytes {
            get 
            {
                return dataBytes;
            }
            set
            {
                dataBytes = value;
            }
        }
        private IList<byte> dataBytes = new List<byte>();
        public byte DLC { get; set; }
        public int ID { get; set; }
        public double Timestamp { get; set; }

        public DataType Type { get; set; }

        #region Not Implemented
        public string ChannelPathName => throw new NotImplementedException();
        public IAttributes Attributes { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public ContainerDataType ContainerType => throw new NotImplementedException();

        public PrimitiveDataType ElementType => throw new NotImplementedException();

        #endregion
    }
}
