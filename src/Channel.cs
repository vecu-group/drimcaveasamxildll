﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ASAM.XIL.Interfaces.Testbench.NetworkPort;
using ASAM.XIL.Interfaces.Testbench.NetworkPort.Enum;

namespace DrimcaveXilAPI
{
    public class Channel : IChannel
    {
        public INetworkPort networkPort;
       // NetworkStream TcpStream;
        public Channel(Cluster parent)
        {
            Cluster = parent;
            networkPort = Cluster.NetworkPort;
        }

        public ICluster Cluster { get; private set; }
        
        public IList<IFrame> Frames { 
            get 
            {
                return frames;
            } 
        }

        private IList<IFrame> frames = new List<IFrame>();

        public string Name { get; set; }


        public IFrame GetFrameByFrameName(string frameName)
        {
            NetworkPort.SendStatus("1,1,1,[ASAMXIL] IChannel GetFrameByFrameName();");
            foreach (Frame f in Frames)
            {
                if (f.Name.Equals(frameName))
                    return f;
            }
            return null;
        }

        public IFrame GetFrameById(int id)
        {
            NetworkPort.SendStatus("1,1,1,[ASAMXIL] IChannel GetFrameById();");
            foreach (Frame f in Frames)
            {
                if (f.ID == id)
                    return f;
            }
            Frame newFrame = new Frame(this);
            newFrame.ID = id;
            Frames.Add(newFrame);
            return newFrame;
        }

        void UpdateFrames(string s)
        {
            //0x102,8,0xA1 0xB2 0xC3 0xD4 0xE5 0xF6 0x07
            string[] arr = s.Split(',');
            if (arr.Length != 3)
                return;
            //double ts = Convert.ToDouble(arr[0]);
            int id = Convert.ToInt32(arr[0], 16);
            byte dlc = 8; // fixed 8
            string[] arr_data = arr[2].Split(' ');

            Frame f = (Frame)GetFrameById(id);

            f.CustomValue.ID = id;
            f.CustomValue.DLC = dlc;
            f.CustomValue.Timestamp = 0;
            if (arr_data.Length >= 8)
            {
                for (int i = 0; i < arr_data.Length; i++)
                {
                    byte data = Convert.ToByte(arr_data[i], 16);
                    f.CustomValue.DataBytes[i] = data;
                }
            }
        }

        void UpdateRxFrames(string s)
        {
            //0x102,8,0xA1 0xB2 0xC3 0xD4 0xE5 0xF6 0x07
            string[] arr = s.Split(',');
            if (arr.Length != 3)
                return;
            //double ts = Convert.ToDouble(arr[0]);
            int id = Convert.ToInt32(arr[0], 16);
            byte dlc = 8; // fixed 8
            string[] arr_data = arr[2].Trim().Split(' ');

            Frame f = (Frame)GetFrameById(id);
            f.Receiver.IsActive = true;
            f.CustomValue.ID = id;
            f.CustomValue.DLC = dlc;
            f.CustomValue.Timestamp = 0;
            if (arr_data.Length >= 8)
            {
                for (int i = 0; i < arr_data.Length; i++)
                {
                    byte data = Convert.ToByte(arr_data[i], 16);
                    f.CustomValue.DataBytes[i] = data;
                }
            }
        }

        Thread threadTx;
        Thread threadRx;
        bool runThreadTx;
        bool runThreadRx;
        void runReceive()
        {
            byte[] buf = new byte[1024];
            while (runThreadRx)
            {
                int nbytes = 0;
                try
                {
                    nbytes = NetworkPort.TcpStream.Read(buf, 0, buf.Length);
                }
                catch
                {
                    Console.WriteLine("#error");
                    return;

                }
                string output = Encoding.ASCII.GetString(buf, 0, nbytes);
                if (output.Equals(""))
                    continue;
                UpdateRxFrames(output);
                Console.WriteLine(output);
            }
        }
        void runSend()
        {
            while (runThreadTx)
            {
                foreach (Frame f in Frames)
                {
                    if(f.Sender.IsActive)
                    {
                        f.Sender.SendOnce();
                    }
                }
                Thread.Sleep(100);
            }
        }

        public void StartReceiving()
        {
            NetworkPort.SendStatus("1,1,1,[ASAMXIL] IChannel StartReceiving();");
            if (networkPort.State != NPState.eRUNNING)
            {
                Console.WriteLine("Fail StartReceiving");
                return;
            }
            Console.WriteLine("StartReceiving");
            runThreadRx = true;
            threadRx = new Thread(new ThreadStart(runReceive));
            threadRx.IsBackground = true;
            threadRx.Start();

        }

        public void StartSending()
        {
            NetworkPort.SendStatus("1,1,1,[ASAMXIL] IChannel StartSending();");
            if (networkPort.State != NPState.eRUNNING)
            {
                Console.WriteLine("Fail StartSending");
                return;
            }
            Console.WriteLine("StartSending");
            runThreadTx = true;
            threadTx = new Thread(new ThreadStart(runSend));
            threadTx.IsBackground = true;
            threadTx.Start();
        }

        public void StopReceiving()
        {
            NetworkPort.SendStatus("1,1,1,[ASAMXIL] IChannel StopReceiving();");
            Console.WriteLine("StopReceiving");
            Thread endThread = new Thread(() =>
            {
                runThreadRx = false;
                threadRx.Join(8000);
            });
            endThread.IsBackground = true;
            endThread.Start();

        }

        public void StopSending()
        {
            NetworkPort.SendStatus("1,1,1,[ASAMXIL] IChannel StopSending();");
            Console.WriteLine("StopSending");
            Thread endThread = new Thread(() =>
            {
                runThreadTx = false;
                threadTx.Join(8000);
            });
            endThread.IsBackground = true;
            endThread.Start();
        }

        #region Not Implemented

        public IFrameCapture CreateFrameCapture()
        {
            throw new NotImplementedException();
        }

        public ISignalCapture CreateSignalCapture()
        {
            throw new NotImplementedException();
        }

        public IPdu GetPdu(string pathName)
        {
            throw new NotImplementedException();
        }

        public ISignal GetSignal(string pathName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
