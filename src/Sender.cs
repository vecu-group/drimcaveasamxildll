﻿using ASAM.XIL.Interfaces.Testbench.NetworkPort;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrimcaveXilAPI
{
    public class Sender : ISender
    {
        Frame parent;
        public Sender(Frame f)
        {
            parent = f;
            IsActive = false;
        }


        public void SendOnce()
        {
            string msg = parent.ToString2();
            byte[] buff = Encoding.ASCII.GetBytes(msg);
            try
            {
                NetworkPort.TcpStream.Write(buff, 0, buff.Length);
                //System.Console.WriteLine(msg);
                NetworkPort.SendStatus($"1,1,1,[ASAMXIL] ISender SendOnce()[{parent.ToString3()}];");
            }
            catch
            {
                NetworkPort.SendStatus("1,1,1,[ASAMXIL] Failed SendOnce.");
            }
        }

        public bool IsActive { get; set; }

        #region Not Implemented

        public IList<IFrameValueSequence> Sequences => throw new NotImplementedException();

        public double CycleTime { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        
        public long Repeats { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public bool IsStarted => throw new NotImplementedException();

        public IFrameValueSequence AddNewSequence()
        {
            throw new NotImplementedException();
        }

        public void ClearSequences()
        {
            throw new NotImplementedException();
        }
        public void StartSequences(long repeats)
        {
            throw new NotImplementedException();
        }
        public void StopSequences()
        {
            throw new NotImplementedException();
        }

        public void StartSending()
        {
            throw new NotImplementedException();
        }

        public void StopSending()
        {
            throw new NotImplementedException();
        }


        #endregion
    }
}
