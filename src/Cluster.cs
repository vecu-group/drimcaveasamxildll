﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASAM.XIL.Interfaces.Testbench.NetworkPort;
using ASAM.XIL.Interfaces.Testbench.NetworkPort.Enum;

namespace DrimcaveXilAPI
{
    public class Cluster : ICluster
    {
        public Cluster(NetworkPort parent)
        {
            BusType = NPBusType.eCAN;
            Name = "vCAN";
            networkPort = parent;
            channels.Add(new Channel(this) { Name = "vCAN0" });

        }

        public string Name { get; }
        public NPBusType BusType { get; private set; }

        public IList<IChannel> Channels {
            get 
            {
                return channels;
            }
        }
        private IList<IChannel> channels = new List<IChannel>();

        public INetworkPort NetworkPort
        {
            get
            {
                return networkPort;
            }
        }
        NetworkPort networkPort;

        public IChannel GetChannel(string channelName)
        {
            foreach(Channel ch in Channels)
            {
                if (ch.Name.Equals(channelName))
                    return ch;
            }
            return null;
        }

        #region Not Implemented

        public IFrameCapture CreateFrameCapture()
        {
            throw new NotImplementedException();
        }

        public ISignalCapture CreateSignalCapture()
        {
            throw new NotImplementedException();
        }

        public IFrame GetFrameByIdAndChannelName(int id, string channelName)
        {
            throw new NotImplementedException();
        }

        public IFrame GetFrameByPathName(string pathName)
        {
            throw new NotImplementedException();
        }

        public IPdu GetPdu(string pathName)
        {
            throw new NotImplementedException();
        }

        public ISignal GetSignal(string pathName)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
