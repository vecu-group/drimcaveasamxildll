* * *
"""본 저장소는 과학기술통신부의 재원으로 정보통신기획평가원(IITP)의 지원을 받아 (주) 드림에이스, 경북대학교, 대구경북과학기술원이 함께 수행한 "차량 ECU 응용소프트웨어 개발 및 검증자동화를 위한 가상 ECU기반 차량레벨 통합 시뮬레이션 기술개발(과제고유번호: 2710008421)" 과제의 일환으로 공개된 오픈소스 코드입니다. (2022.04~2024.12)"""

"""본 저장소는 AsamXil Dll 프로젝트를 위한 것으로 Drimecave에서 ASAM-XIL에 대한 DLL을 개발하여 외부 툴을 이용하여 가상 ECU 테스트 환경을 개발하기 위한 용도로 활용합니다."""
* * *

# DrimcaveAsamXilDll


## 개발환경
- IDE : Visual Studio 2019 (Community) 
 - License : 상용화시 업데이트 필요

- ASAMXIL Interface 연결 필요





## 개발 목표

ECU 통신 테스트를 할 수 있는 외부 툴(CANoe , 시뮬링크 등)를 이용하여 가상 ECU 테스트 환경 개발에 필요한 ASAM XIL Interface **DLL(이하 AsamxilDLL)** 개발하여 Test에 필요한 통신 Interface를 제공한다.

![Untitled](doc/Untitled.png)

<aside>
👉 그림의 빨간 선은 자동 검증 테스트 환경에서 ASAMXIL DLL의 개발 범위를 나타낸다

</aside>

## 방법론

### AsamxilDLL의 역할

**AsamxilDLL**는 ASAM XIL Interface의 내부 동작이 구현된 DLL이다.

![Untitled](doc/Untitled%201.png)

일반적으로 외부 툴에서 H/W Interface에 의존적이지 않도록 H/W vendor에서 제공하는 ASAM XIL Interface를 사용한다. 각 벤더사의 **AsamxilDLL** 내부에서는 H/W Driver와 연결하는 역할을 한다. 

본 과제에서도 외부 툴의 테스트 환경에서 ASAM XIL Interface를 사용해야 하며, 제공되는 **AsamxilDLL**로 가상 ECU에 연결된 vCAN과 통신해야 한다.

이 때 외부 툴의 구동 환경은 Windows, vECU 환경은 Linux(wsl)이기 때문에 **AsamxilDLL** 내부에서는 TCP/IP로 통신한다.

![Untitled](doc/Untitled%202.png)

<aside>
👉 CAN신호는 TCP/IP를 통해 Server에 데이터를 전달하며 Server는 레드가 현대 오토에버 건으로 개발한 TCP Server를 사용 (231018 회의)

</aside>

![Untitled](doc/Untitled%203.png)

### AsamxilDLL의 내부 역할과 구조 (메타몽 이 부분 넘어가셔도 됩니다.)

![Untitled](doc/Untitled%204.png)

ASAM XIL Interface 구조에서 CAN 통신을 위해 Network Port Interface를 제공한다.

개발 기간의 제약으로 AsamxilDLL에서는 일부만 사용하여 본 과제 환경에 맞춰 개발하게 되었다.

- 전체 Network Port Interface List
    
    ![Untitled](doc/Untitled%205.png)
    
- 개발된 Interface 구조와 역할
    
    ![Untitled](doc/Untitled%206.png)
    

<aside>
👉 TODO : 내부 클래스별 함수와 기능 정리 필요

</aside>

### CANoe Testcase Sample를 통한 AsamxilDLL 사용 예시

CANoe Testcase 제작 시 .net(C#)으로 개발할 수 있고 ASAM XIL Interface를 불러와 TCP/IP 통신하는 Sample 코드를 설명한다.

<aside>
👉 CANoe 구성 세팅과 문법은 설명하지 않음

</aside>

1. Import ASAM XIL Interface 
    
    ![Untitled](doc/Untitled%207.png)
    
2. Interface에서 AsamxilDLL loading 및 NetworkPort 구성
    
    ![Untitled](doc/Untitled%208.png)
    
    ![Untitled](doc/Untitled%209.png)
    
    - TestbenchFactory에 AsamxilDLL  정보가 있는 imf파일 경로 전달
    - CreateVendorSpecificTestbench 를 통해 testbench 생성
    - CreateNetworkPort를 통해 NetworkPort 객체 얻음
    - sNetPort.Start()로 TCP 통신 시작
    
    ![Untitled](doc/Untitled%2010.png)
    
3. CAN Msg 송신 예시
    
    ![Untitled](doc/Untitled%2011.png)
    
    - SendOnce를 통해 Msg 한번 전송
    - 주석된 내용은 Cyclic하게 송신할 수 있는 방법(f2변수)
4. CAN Msg 수신 예시
    
    ![Untitled](doc/Untitled%2012.png)
    
    - f에는 수신 후 업데이트 된 data를 가지고 있음
5. 통신 종료
    
    ![Untitled](doc/Untitled%2013.png)
    
    - TCP 연결 종료

<aside>
👉 Testcase 개발할 때 위와 같이 송수신 사용을 적용 해야 함

</aside>

<aside>
👉 TODO : Server Log 창

</aside>

## 구현 된 ASMXIL DLL API

- ITestbench
    - BuildNumber : 빌드 버전
    - NetworkPortFactory : INetworkPort 생성
    - MajorNumber : 빌드 버전의 Major Number
    - MinorNumber : 빌드 버전의 Minor Number
    - ProductName : 제품 이름
    - ProductVersion : 제품 버전
    - RevisionNumber : Revision Number
    - VendorName : Vendor 이름
- INetworkPortFactory
    - CreateNetworkPort : INetworkPort 생성
- INetworkPort
    - Clusters : 자식 Cluster 객체를 얻음, Cluster는 통신 프로토콜 구분으로 사용됨
    - State : Run 상태
    - Start : 통신 시작
    - Stop : 통신 정지
    - Disconnect : 통신 종료
- ICluster
    - Name : Bus의 이름
    - BusType : Bus 프로토콜 타입
    - Channels : 자식 Channel 객체를 얻음
    - NetworkPort : 부모의 NetworkPort 객체를 얻음
    - GetChannel : Channel 이름으로 Channel 객체를 얻음
- IChannel
    - Cluster : 부모의 Cluster 객체를 얻음
    - Frames : 자식 Frame 객체를 얻음
    - Name : Channel의 이름
    - GetFrameByFrameName  : Frame 이름으로 Frame 객체를 얻음
    - GetFrameById : Frame ID로 Frame 객체를 얻음
    - StartReceiving : Channel의 수신 시작
    - StartSending : Channel의 송신 시작
    - StopReceiving : Channel의 수신 정지
    - StopSending : Channel의 송신 정지
- IFrame
    - CustomValue : IFrameValue 객체를 얻음, Frame의 Data 정보를 담겨 있음
    - DLC : Frame의 DLC
    - ID : Frame의 ID
    - Name : Frame의 이름
    - Sender : 자식 Sender 객체를 얻음
- ISender
    - SendOnce : 부모 IFrame의 데이터를 전송
    - IsActive : 활성화 상태 여부
- IFrameValue
    - DataBytes : Frame의 Data
    - DLC : Frame의 DLC
    - ID : Frame의 ID
    - Timestamp : Frame의 타임스탬프
    - Type : Data Type을 의미하고 eUINT로 고정

## 결과

<aside>
👉 TODO : 결과물 DLL 과 테스트 환경 업로드

</aside>

CANoe의 AsamxilDLL를 사용한 Testcase에서 vcan까지 송신되는 결과물

- server tcp to can 변환 개선 필요

![Untitled](doc/Untitled%2014.png)

- 경북대 전달
    - 이전파일
        
        [경북대 전달.zip](doc/%25EA%25B2%25BD%25EB%25B6%2581%25EB%258C%2580_%25EC%25A0%2584%25EB%258B%25AC.zip)
        
    
    [231016_TestCANoeASAMDLL.zip](doc/231016_TestCANoeASAMDLL.zip)
    
    - 사용법
        1. CANoe로 하기 경로 cfg 실행
            
            .\CANoe_Config\Configuration1.cfg 
            
        2. Server 실행(ip: 127.0.0.1 , port: 11180)
            1. 현재 ip,port 하드코딩 되어있기 때문에 변경 필요 시 문의 
        3. 캡처 번호와 같이 실행
            
            ![Untitled](doc/Untitled%2015.png)
            
    - 시뮬레이션 설명
        - CANoe config는 ASAM XIL Interface API를 활용한 Testcase 예제
        - CANoe는 tcp client역할
        - testcase 코드 설명
            - Init : TCP 연결 및 초기화 함수
            - TcpIpSendTestTask : CANoe에서 Send 예제
            - TcpIpRecevieTest : CANoe에서 Receive 예제
            - Shutdown : TCP disconnect
    - TCP Data format
        
        :ID(hex),DLC(dec),Data1 Data2 … Data8
        
        :0x201,8,0x14 0x22 0x33 0x44 0x55 0x66 0x77 0x88
        

## 참고

- CANoe에서 C#을 사용한 Testcase 예시(AsamxilDLL 사용x)
    - CANoe에서 Test는 TestUnits과 Test Modules 두 종류이며, 캡처에서는 Test Modules에 대한 예시
        
        ![Untitled](doc/Untitled%2016.png)
        
    
    - .net (C#) 코드 예시
        
        ![Untitled](doc/Untitled%2017.png)
        
- ASAM XIL Network Port Flowchart(개발 내용x)
    
    ![Untitled](doc/Untitled%2018.png)
    
    ![Untitled](doc/Untitled%2019.png)
    
    ![Untitled](doc/Untitled%2020.png)
    
    ![Untitled](doc/Untitled%2021.png)
    
    ![Untitled](doc/Untitled%2022.png)
    
    ![Untitled](doc/Untitled%2023.png)
    

## 사용법 요약

```csharp
INetworkPort sNetPort;
string sManifestFolder = "../asamxil_imf";
string sVendorName = "DRIMAES";
string sProductName = "DRIMCAVE";
string sProductVersion = "1.0.0.0";

//1
TestbenchFactory factory = new TestbenchFactory(sManifestFolder);

//2
ITestbench testbench = factory.CreateVendorSpecificTestbench(sVendorName, sProductName, sProductVersion);
sNetPort = testbench.NetworkPortFactory.CreateNetworkPort("my");

//3
sNetPort.Start(); 

//4
int i = 0;
while(i++ < 20)
{
	if (sNetPort.State == ASAM.XIL.Interfaces.Testbench.NetworkPort.Enum.NPState.eRUNNING)
		break;
	Thread.Sleep(1000);
}

//5
IChannel ch = sNetPort.Clusters[0].Channels[0];
ch.StartReceiving();

//6
List<byte> data = new List<byte>();
data.Add(0x0);
data.Add(0x0);
data.Add(0x1);
data.Add(0x1);

data.Add(0xFF);
data.Add(0xFF);
data.Add(0xFF);
data.Add(0xFF);
			
ASAM.XIL.Interfaces.Testbench.NetworkPort.IFrame f = ch.GetFrameById(0xC8);
f.CustomValue.DataBytes = data;
f.Sender.SendOnce();

//7
Thread.Sleep(500);
ch.StopReceiving();
sNetPort.Stop();
```

1. ASAMXIL DLL 경로 설정
2. ASAMXIL DLL 호출 및 초기화
3. TCP/IP Server와 연결( port 11190 , 11180)
    1. 통신 server : 11180
    2. 상태 전달 server : 11190
4. 통신  server 연결 대기
5. 수신 thread 시작
6. 송신 데이터 입력 및 전송
7. TCP 통신 종료
